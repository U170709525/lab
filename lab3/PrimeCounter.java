public class PrimeCounter {

	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		for (int count = 2; count <a; count++) {
			boolean isPrime = true;
			for (int divider = 2; divider <= Math.sqrt(count); divider++) {
				if (count % divider == 0) {
					isPrime = false;
				}
			}
			if (isPrime) {
				System.out.println(count);
			}
		} 
	}
}