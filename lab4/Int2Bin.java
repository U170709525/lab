
public class Int2Bin {

	public static void main(String[] args) {
		System.out.println(int2Bin(98));
	}
	
	
	private static String int2Bin(int num) {
		if (num == 0 || num == 1)
			return num + "" ; 

		return int2Bin(num/2) + (num % 2); 
	}
 }
 
 
